package com.android.minhnt.livedataroom;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.TypeConverters;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

/**
 * Created by PC on 5/9/2018.
 */

@Dao
public interface BorrowDao {
    @Query("select * from Borrow")
    LiveData<List<Borrow>> getAllBorrowedItems();

    @Query("select * from Borrow where id = :id")
    Borrow getItemById(int id);

    @Insert(onConflict = REPLACE)
    void addItem(Borrow borrow);

    @Delete
    void deleteItem(Borrow borrow);
}
