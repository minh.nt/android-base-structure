package com.android.minhnt.livedataroom;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

/**
 * Created by PC on 5/9/2018.
 */

@Database(entities = {Borrow.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    private static AppDatabase instance;

    public static AppDatabase getInstance(Context context) {
        if (instance == null) {
            instance = Room.databaseBuilder(context, AppDatabase.class, "borrow_db").build();
        }

        return instance;
    }

    public abstract BorrowDao getBorrowModelDao();
}
