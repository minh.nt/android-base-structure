package com.android.minhnt.livedataroom;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;
import android.support.annotation.NonNull;

import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Created by PC on 5/9/2018.
 */

public class BorrowViewModel extends AndroidViewModel {
    private AppDatabase appDatabase;

    public BorrowViewModel(@NonNull Application application) {
        super(application);
        appDatabase = AppDatabase.getInstance(this.getApplication());
    }

    public LiveData<List<Borrow>> getListBorrowModels() {
        return appDatabase.getBorrowModelDao().getAllBorrowedItems();
    }

    public Borrow getItemById(int id) {
        Borrow borrow = null;
        try {
            borrow = new GetItemByIdAsyncTask(appDatabase).execute(id).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return borrow;
    }

    public void insertItem(Borrow borrow) {
        new InsertAsyncTask(appDatabase).execute(borrow);
    }

    public void deleteItem(Borrow borrow) {
        new DeleteAsyncTask(appDatabase).execute(borrow);
    }

    private static class DeleteAsyncTask extends AsyncTask<Borrow, Void, Void> {

        private AppDatabase db;

        DeleteAsyncTask(AppDatabase appDatabase) {
            db = appDatabase;
        }

        @Override
        protected Void doInBackground(final Borrow... params) {
            db.getBorrowModelDao().deleteItem(params[0]);
            return null;
        }

    }

    private static class InsertAsyncTask extends AsyncTask<Borrow, Void, Void> {

        private AppDatabase db;

        InsertAsyncTask(AppDatabase appDatabase) {
            db = appDatabase;
        }

        @Override
        protected Void doInBackground(final Borrow... params) {
            db.getBorrowModelDao().addItem(params[0]);
            return null;
        }

    }

    private static class GetItemByIdAsyncTask extends AsyncTask<Integer, Void, Borrow> {

        private AppDatabase db;

        GetItemByIdAsyncTask(AppDatabase appDatabase) {
            db = appDatabase;
        }

        @Override
        protected Borrow doInBackground(final Integer... params) {
            return db.getBorrowModelDao().getItemById(params[0]);
        }

    }
}
