package com.android.minhnt.livedataroom;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.minhnt.livedataroom.databinding.ItemMainBinding;

import java.util.List;

/**
 * Created by PC on 5/9/2018.
 */

public class MainAdapter extends RecyclerView.Adapter<MainAdapter.MyVH> {
    private List<Borrow> borrowList;
    private View.OnLongClickListener onLongClickListener;
    private View.OnClickListener onClickListener;

    public MainAdapter(List<Borrow> borrowList, View.OnLongClickListener onLongClickListener, View.OnClickListener onClickListener) {
        this.borrowList = borrowList;
        this.onLongClickListener = onLongClickListener;
        this.onClickListener = onClickListener;
    }

    @NonNull
    @Override
    public MyVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyVH(ItemMainBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyVH holder, int position) {
        holder.setData(borrowList.get(position));
        holder.itemView.setTag(borrowList.get(position));
        holder.itemView.setOnLongClickListener(onLongClickListener);
        holder.itemView.setOnClickListener(onClickListener);
    }

    @Override
    public int getItemCount() {
        return borrowList.size();
    }

    public void setBorrowList(List<Borrow> borrowList) {
        this.borrowList = borrowList;
        notifyDataSetChanged();
    }

    class MyVH extends RecyclerView.ViewHolder {
        private ItemMainBinding binding;

        public MyVH(ItemMainBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void setData(Borrow data) {
            binding.setBorrow(data);
            binding.executePendingBindings();
        }
    }
}
