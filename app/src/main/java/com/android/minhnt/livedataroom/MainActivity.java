package com.android.minhnt.livedataroom;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnLongClickListener, View.OnClickListener {
    private RecyclerView rvBorrow;
    private MainAdapter adapter;
    private BorrowViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        rvBorrow = findViewById(R.id.rvBorrow);
        rvBorrow.setLayoutManager(new LinearLayoutManager(this));
        rvBorrow.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
                super.getItemOffsets(outRect, view, parent, state);
                outRect.set(10, 10, 10, 10);
            }
        });
        viewModel = ViewModelProviders.of(this).get(BorrowViewModel.class);
        adapter = new MainAdapter(new ArrayList<Borrow>(), this, this);
        rvBorrow.setAdapter(adapter);

        viewModel.getListBorrowModels().observe(MainActivity.this, new Observer<List<Borrow>>() {
            @Override
            public void onChanged(@Nullable List<Borrow> borrows) {
                Log.d("hihi", borrows.size() + "");
                adapter.setBorrowList(borrows);
            }
        });

        findViewById(R.id.btnAdd).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, AddActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onLongClick(View v) {
        Borrow borrow = (Borrow) v.getTag();
        viewModel.deleteItem(borrow);
        Toast.makeText(this, "Đã xóa!", Toast.LENGTH_SHORT).show();
        return true;
    }

    @Override
    public void onClick(View v) {
        Borrow borrow = (Borrow) v.getTag();
        viewModel.getItemById(borrow.id);
        Toast.makeText(this, borrow.id + " - " + borrow.getItemName() + " - " + borrow.getBorrowDate().toLocaleString(), Toast.LENGTH_SHORT).show();
    }
}
