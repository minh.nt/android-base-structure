package com.android.minhnt.livedataroom;

import android.arch.persistence.room.TypeConverter;
import android.util.Log;

import java.util.Date;

/**
 * Created by PC on 5/9/2018.
 */

public class DateConverter {

    @TypeConverter
    public static Date toDate(Long timestamp) {
        return timestamp == null ? null : new Date(timestamp);
    }

    @TypeConverter
    public static Long toTimestamp(Date date) {
        return date == null ? null : date.getTime();
    }
}
