package com.android.minhnt.livedataroom;

import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Date;

public class AddActivity extends AppCompatActivity {
    private EditText etItemName, etPersonName;
    private BorrowViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);
        etItemName = findViewById(R.id.etItemName);
        etPersonName = findViewById(R.id.etPersonName);
        viewModel = ViewModelProviders.of(this).get(BorrowViewModel.class);
        findViewById(R.id.btnOk).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewModel.insertItem(new Borrow(etItemName.getText().toString(), etPersonName.getText().toString(), new Date()));
                Toast.makeText(AddActivity.this, "Added", Toast.LENGTH_SHORT).show();
                finish();
            }
        });
    }
}
